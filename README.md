# README #

Simple script to temporarily drive all users to a single page landing page instead of your full website.

### What is this repository for? ###

* Perfect for when you are redesigning a website that is outdated and want a temporary placeholder.

### How do I get set up? ###

One of three ways:

1. Upload the du_landing_page_redirect.php file to your plugins folder.
2. Require it through composer
3. Copy the file to your theme and INCLUDE it.

### Once it is installed and active ### 

Toggle the landing page redirect on/off by logging into your WordPress site as an admin and visiting:

  http://your_site_url.com/?du_landing_page_redirect=status

From that screen, you can choose to turn the landing page redirect on or off.

### Selecting your Landing Page Template ###

By default, the template that is loaded will be /wp-content/themes/your-theme/tpl-landing-page.php.

You can modify the template pulled by using the du_landing_page_template_path filter:

```php

add_filter( 'du_landing_page_template_path', 'landing_page_template' );

# Replace your-template-file.php with the name of your landing page template
function landing_page_template( $landing_template ) {
  $landing_template = TEMPLATEPATH . '/your-template-file.php';
  return $landing_template;
}
```

### Who do I talk to? ###
Developed by duable.com