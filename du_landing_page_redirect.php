<?php
/*
Plugin Name: Du Landing Page Redirect
Plugin URI: https://duable.com/
Description: Redirect all visitors to a landing page template - either temporarily or permanently.
Version: 0.9
Author: Duable <dev@duable.com>
Author URI: https://duable.com/
*/

namespace du;

/**
 * Landing Page Redirect Script
 * 
 * Toggle landing page.  If active, only
 * allow viewers to view landing page.
 *
 * @package du
 * @author dev@duable.com
 **/
class landing_page_redirect {

  function __construct() {
    if ( is_super_admin() && !empty( $_GET[ 'du_landing_page_redirect' ] ) )
      $this->toggle_landing_page();

    # Activate Landing Page
    if ( get_option( 'du_landing_page_redirect' ) == true )
      add_action( 'template_redirect', array( $this, 'landing_page_redirect' ) );

    if ( landing_page_redirect::status() )
      add_filter( 'du_css_files', array( $this, 'load_css' ) );
  }

  function load_css( $css_files ) {
    $css_file = "/assets/css/landing.css";
    $css_files = $css_files + array( 'landing-page' => $css_file );
    return $css_files;
  }

  /**
   * Check if landing page redirect is on
   */
  static function status(){
    if ( get_option( 'du_landing_page_redirect' ) == true ) 
      return true;

    return false;
  }

  /**
   * Toggle landing page with url parameters
   * 
   * @return null
   * @author mohammad@duable.com
   */
  public function toggle_landing_page() {
    $toggle = $_GET[ 'du_landing_page_redirect' ];
    # Let's turn on redirect
    if ( $toggle == 'true' ) :
      update_option( 'du_landing_page_redirect', true );
    # Let's turn off redirect
    elseif ( $toggle == 'false' ) :
      update_option( 'du_landing_page_redirect', false );
    # No toggle option set so let's show status page
    else :
      $current = get_option( 'du_landing_page_redirect' );
      $status = 'Inactive';
      if ( $current == true ) 
        $status = 'Active';
      ?>
      <section class="toggle-du-landing-page-redirect" style="text-align: center">
        <h1>Landing page is currently: <?php echo $status; ?></h1>
        <a href="<?php echo home_url(); ?>?du_landing_page_redirect=true">Turn On</a>
        <a href="<?php echo home_url(); ?>?du_landing_page_redirect=false">Turn Off</a>
      </section>
      <?php
      die();
    endif;
  }

  /**
   * If landing page is active, redirect all traffic
   * to landing page.
   * 
   * @return null
   * @author mohammad@duable.com
   */
  public function landing_page_redirect() {
    global $post;

    # Default landing page template path
    $landing_template = TEMPLATEPATH . '/tpl-landing-page.php';

    # Allow Filtering of template path
    $landing_template = apply_filters( 'du_landing_page_template_path', $landing_template );

    # Filter for pages to allow redirecting to
    $allowed_pages = array();
    $allowed_pages = apply_filters( 'du_landing_redirect_allowed_pages', $allowed_pages );

    # If not on front page, redirect there
    if ( !is_front_page() && !in_array( $post->post_name, $allowed_pages ) )
      header( "Location: " . home_url() );

    # If on front page, load the landing page template
    if ( is_front_page() && file_exists( $landing_template ) ) :
      include_once $landing_template;
      die();
    endif;

    return false;

  }

} new landing_page_redirect;